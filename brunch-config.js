module.exports = {
  files: {
    javascripts: {
      joinTo: {
        'app.js': /^app/,
        'vendor.js': /^(?!app)/
      }
    },
    stylesheets: {
      joinTo: {
        'app.css': /^app\//  
      }
    }
  },
  plugins: {
    sass: {
      allowCache: true,
      options: {
        includePaths: ['node_modules/foundation-sites/scss']
      }
    }
  }
}